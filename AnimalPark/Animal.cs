﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalPark
{
    class Animal
    {
        private String name;
        private String gender;
        private int cuteness;

        public Animal(String name, String gender, int cuteness)
        {
            this.name = name;
            this.gender = gender; 
            this.cuteness = cuteness;
        }


        public string Name { get => name; set => name = value; }
        public int Cuteness { get => cuteness; set => cuteness = value; }
        public string Gender { get => gender; set => gender = value; }

        public void Pet()
        {
            if (cuteness >= 7)
            {
                Console.WriteLine($"You pet the {name}! What a good {Gender.ToLower()}");
            } else if (cuteness <= 4) {
                Console.WriteLine($"You can't pet the {name} because it is too ugly");
            }
        }
    }
}
