﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AnimalPark
{
    class AnimalsEnum : IEnumerator
    {

        public Animal[] animals;
        int position = -1;

        public AnimalsEnum(Animal[] animals)
        {
            this.animals = animals;
        }

        public Animal Current
        {
            get
            {
                try
                {
                    return animals[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }



        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public bool MoveNext()
        {
            position++;
            return (position < animals.Length);
        }

        public void Reset()
        {
            position = -1;
        }
    }
}
