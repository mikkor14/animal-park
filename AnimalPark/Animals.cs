﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace AnimalPark
{
    class Animals : IEnumerable
    {
        private Animal[] animals;

        public Animals(Animal[] animals)
        {
            this.animals = animals;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public AnimalsEnum GetEnumerator()
        {
            return new AnimalsEnum(animals);
        }


    }
}
