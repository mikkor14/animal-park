﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace AnimalPark
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal Cat = new Animal("Cat", "Girl", 8);
            Animal Dog = new Animal("Dog", "Boy", 7);
            Animal Crocodile = new Animal("Crocodile", "Boy", 4);
            Animal Blobfish = new Animal("Blobfish", "Girl", 1);
            Animal Quokka = new Animal("Quokka", "Boy", 10);

            Animal[] animals = new Animal[5] { Cat, Dog, Crocodile, Blobfish, Quokka };
            Animals animalList = new Animals(animals);

            foreach (Animal a in animalList)
            {
                Console.WriteLine($"{a.Name} {a.Gender}");
            }

        }
    }
}
